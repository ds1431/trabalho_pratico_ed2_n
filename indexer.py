import argparse
import os
import re
import math
import time
from collections import Counter


# Função para contar palavras em um arquivo:
def count_words_in_file(file_path):
    word_count = Counter() # Conta a ocorrência de elementos

    # Arquivo é aberto em modo de leitura binária
    with open(file_path, 'rb') as file:
        text = file.read().lower().decode('utf-8', 'ignore')
        # O conteúdo é lido, convertido para minúsculas e decodificado de bytes para uma string usando UTF-8
        
        words = re.findall(r'\b\w+\b', text)
        # Extrai todas as palavras (sequências de caracteres alfanuméricos) da string text usando uma expressão regular 

        # Lista compreensiva é usada para filtrar palavras 
        words = [word for word in words if len(word) >= 2]
        word_count.update(words)
        # Contador é atualizado com a lista filtrada de palavras

    return word_count


# Função para calcular o TF-IDF de um termo em um documento:
def tfidf(term, word_count, document_index):
    # Cálculo do Term Frequency (TF)
    tf = word_count[term] / sum(word_count.values())

    # Cálculo do Inverse Document Frequency (IDF)
    idf = math.log(1 + (len(document_index)) / sum(1 for doc in document_index.values() if term in doc))
    return tf * idf


# Função para calcular a contagem de palavras e adicionar ao índice do documento
def index_document(file_path, document_index):
    word_count = count_words_in_file(file_path)
    document_index[file_path] = word_count
    # Dicionário onde a chave é o caminho do arquivo e o valor é a contagem de palavras no doc


# Função para buscar documentos relevantes para um termo:
def search(term, document_index):
    results = {} # Dicionário

    for document, word_count in document_index.items():
        results[document] = tfidf(term, word_count, document_index)
    sorted_results = dict(sorted(results.items(), key=lambda item: item[1], reverse=True))
    # Ordena os resultados com base na pontuação TF-IDF (ordem decrescente)

    return sorted_results


# Medindo o tempo de execução
def medir_tempo_de_execucao(funcao):
    def tempo_de_execucao_decorador(*args, **kwargs): 

        # *args permite que a função receba uma quantidade variável de argumentos posicionais. Quando a função é chamada, esses argumentos são empacotados em uma tupla chamada args.

        # **kwargs permite que a função receba uma quantidade variável de argumentos nomeados (palavras-chave). Quando a função decorada é chamada, esses argumentos são empacotados em um dicionário chamado kwargs.
        
        tempo_inicio = time.time()
        resultado = funcao(*args, **kwargs)
        tempo_fim = time.time()
        tempo_execucao = tempo_fim - tempo_inicio

        if tempo_execucao >= 60:
            minutos = int(tempo_execucao // 60)
            segundos = tempo_execucao % 60
            print(f"Tempo de execução: {minutos} minuto(s) e {segundos:.3f} segundo(s)")
        else:
            print(f"Tempo de execução: {tempo_execucao:.3f} segundo(s)")

        return resultado
    return tempo_de_execucao_decorador

@medir_tempo_de_execucao


def main():
    # A biblioteca argparse foi usada para processar argumentos da linha de comando

    parser = argparse.ArgumentParser(description='Processa comandos para analisar arquivos.')
    parser.add_argument('--freq', type=int, help='Exibe as N palavras mais frequentes em um arquivo')
    parser.add_argument('--freq-word', help='Exibe a contagem de uma palavra específica em um arquivo')
    parser.add_argument('--search', help='Exibe documentos relevantes para um termo de busca')
    parser.add_argument('arquivos', nargs='*', help='Nomes dos arquivos para analise')

    args = parser.parse_args()
    # Informações são armazenadas no objeto args

    document_index = {} 
    # Dicionário para armazenar a contagem de palavras
    
    for arquivo in args.arquivos:
        index_document(arquivo, document_index)

    if args.freq:
        if len(args.arquivos) == 1:
            document = args.arquivos[0]
            if document in document_index:
                word_count = document_index[document]
                most_common = word_count.most_common(args.freq)
                # most_common retorna uma lista dos elementos mais comuns e suas contagens
                # Obtém as N palavras mais frequentes no documento

                for word, count in most_common:
                    print(f"{word}: {count}")
            else:
                print(f"Arquivo '{document}' não encontrado.")

    if args.freq_word:
        word = args.freq_word
        if word:
            for document, word_count in document_index.items():
                count = word_count.get(word, 0)
                # Obtém a contagem da palavra no documento (ou 0 se não estiver presente)

                print(f"A palavra '{word}' aparece {count} vezes em {document}")
        else:
            print("O argumento '--freq-word' deve ser fornecido com a palavra a ser contada.")

    if args.search:
        term = args.search
        search_results = search(term, document_index)
        print(f"Documentos mais relevantes para '{term}':")
        for document, relevance in search_results.items():
            print(f"{document}: TF-IDF = {relevance}")

# Garante que o código seja executado apenas quando o script é o programa principal
if __name__ == "__main__":
    main()